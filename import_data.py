"""
import logging

logging.debug("Bonjour je suis un message de débug")
logging.info("Et moi d'info")
logging.warning(('Nous sommes en defcon 3'))
"""

import logging
import csv
from os import path 
import sqlite3
import sys


def check_csv_file(csv_filename):
    if path.exists(csv_filename):
        return True
    return False

def starting_SIV_table(cursor, connexion):

    cursor.execute('CREATE TABLE IF NOT EXISTS autos(address VARCHAR(100), carrosserie VARCHAR(100), categorie VARCHAR(100), couleur VARCHAR(100), cylindree INT, date_immat VARCHAR(100), denomination VARCHAR(100), energy VARCHAR(100), firstname VARCHAR(100), immat VARCHAR(100), marque VARCHAR(100), name VARCHAR(100), places INT, poids INT, puissance INT, type_variante_version VARCHAR(100), vin VARCHAR(100) PRIMARY KEY)')    
    connexion.commit()

def insert(connexion, cursor, data):
    cursor.execute('''INSERT OR REPLACE INTO autos (address, carrosserie, categorie, couleur, 
    cylindree, date_immat, denomination, energy, firstname, immat, marque, name, places, poids, puissance, 
    type_variante_version, vin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);''', data)
    
    connexion.commit()

def update(connexion, cursor, data):
    pass


def data_insertion(connexion, cursor, data, counters):
    equal_counter = 0 

    data_to_insert = [data[i] for i in range(17)]

    cursor.execute('SELECT * FROM autos WHERE vin= ?' , (data_to_insert[16], ))
    data_to_verify = cursor.fetchone()

    if data_to_verify is None:
        counters['insert'] = counters['insert'] + 1
        insert(connexion, cursor, data)
    else:
        counters['update'] = counters['update'] + 1
        update(connexion, cursor, data)
     
    return counters


def reading_csvfile():
    
    csv_input = str(input("Which file do you want to import?  "))
    return csv_input


if __name__ == '__main__':

    # Counters est un tableau contenant, à la première case le nombre d'insertions et à la 
    # deuxième le nombre d'updates.

    counters = {
        'insert': 0,
        'update': 0,
    }

    logging.basicConfig(filename="database_log.log", format='%(levelname)s : %(asctime)s %(message)s',level=logging.INFO)
    logging.info("Starting the program")

    connexion = sqlite3.connect('bdd.sqlite3')    
    cursor = connexion.cursor()

    starting_SIV_table(cursor, connexion)
    
    csv_input = reading_csvfile()

    if check_csv_file(csv_input):
        
        logging.info("CSV file found, inserting/updating data...")

        print("\nImporting......\n")
        with open(csv_input, 'r') as csvin:

            csvreader = csv.reader(csvin, delimiter='|')

            for data in csvreader:
                if csvreader.line_num >1 :
                    counters = data_insertion(connexion, cursor, data, counters)        
        
        print("\nData imported ! \n")
        logging.info("%d rows were added, %d rows were updated", counters['insert'], counters['update'])
        
    else:
        logging.warning("Error : CSV file not found.")
        print("Your " + csv_input + " file does not exist! \n")

    logging.info("Program Ended")
    sys.exit(0)

        

   
"""
    for i in range(17):
        if str(data_to_insert[i]) == str(data_to_verify[0][i]):
           equal_counter+=1            

        if equal_counter != 17:
            counters[1]+=1
"""
