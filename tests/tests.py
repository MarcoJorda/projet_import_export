import unittest
import sqlite3
import logging
import csv 
import os
from import_data import * 

class DBTestClass(unittest.TestCase):


    def setUp(self):
        self.connexion = sqlite3.connect('test_db.sqlite3')
        cursor = self.connexion.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS test_Table (testColumn VARCHAR(100) PRIMARY KEY NOT NULL, testColumn2 INT)')
        self.connexion.commit()

    def tearDown(self):
        self.connexion.close()
        os.remove('test_db.sqlite3')


    def test_csv_checking(self):
        self.assertFalse(check_csv_file("marco.csv"))
        self.assertFalse(check_csv_file("marco.txt"))
        self.assertTrue(check_csv_file("auto.csv"))
    """
    #does not work
    def test_insertion(self):
        counters = {
        'insert': 0,
        'update': 0,
        }
        cursor = self.connexion.cursor()

        with open("testinsert.csv",'r') as csvfile:
            reader = csv.reader(csvfile) 
            for data in reader:
                data_insertion(self.connexion, cursor, data, counters)
                self.connexion.commit()

        cursor.execute('SELECT * FROM test_Table')
        data_list = cursor.fetchall()

        for data in data_list:
            self.assertEqual(data, ('1','2','3','4','5','6','7','8','9'))
        """

        
